export default {
    namespaced: true,
    state: {
        postId: '',
    },
    mutations: {
        setpostId: (state, payload) => {
            state.postId = payload
        },
    },
    actions: {
        setpostId: ({
            commit
        }, payload) => {
            commit('setpostId', payload)
        },
    },
    getters: {
        postId: (state) => {
            console.log(state.postId)
            return state.postId
        }
    }
}
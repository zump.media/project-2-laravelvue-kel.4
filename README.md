<h1>FINAL PROJECT </h1>
<br>
<h2>Kelompok 4</h2>
<br>
<h3>Angota Kelompok</h3>

1. Yuliana Aisyah
2. Melisa Christanti
3. Lina Rufaidah
<br>
<h3>Tema Project</h3>

Share-With-Us Media System
<br>
<h3>ERD</h3>
<br>
<img src="/Web Service Laravel/backend-laravel-medsos/public/gambar/erd_sosmed_kita.png">
<br>
<h3>Link Video</h3>
Link Deploy : <a href='https://youtu.be/lo3XXvCyqIU' target='_blank'>Deploy Youtube</a>

<br>
<h3>Link Postman </h3>
Link Deploy : <a href='https://documenter.getpostman.com/view/18394607/UVR7LTjo' target='_blank'>Deploy Postman</a>

<br>
<h3>Link Snapshot Demo Website </h3>
Link Deploy : <a href='https://drive.google.com/drive/folders/1_gk9Ygfpcba4wWqRVDXQXsMuDtY_uQqX?usp=sharing' target='_blank'>Deploy GDrive</a>

<?php

namespace App\Http\Controllers;

use App\PostinganLike;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;


class PostinganLikeController extends Controller
{
    public function __construct()
    {
       return  $this->middleware('auth:api')->only(['store','update','delete']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
             //
            // Validate the request...
            //dd($request);
            //

            $allRequest = $request->all();
        
            $validator = Validator::make($allRequest , [
                'postingan_id' => 'required',
            ]);
    
            if($validator->fails()){
                return response()->json($validator->errors() , 400);
            }
    
            $postinganlike = PostinganLike::create([
                'postingan_id' =>$request->postingan_id
            ]);
      
            if($postinganlike){
                return response()->json([
                    'success'   => true,
                    'message'   => 'Data postinganlike berhasil di create',
                    'data'      =>  $postinganlike
                ], 200);
            }
    
            return response()->json([
                'success'   => false,
                'message'   => 'Data postinganlike gagal dibuat'
            ], 409);
 //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

<?php

namespace App\Http\Controllers;

use App\User;
use App\Profile;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class ProfileController extends Controller
{
    public function __construct()
    {
        return $this->middleware('auth:api')->only(['store' , 'update' , 'delete']);
    }

    public function index()
    {
        $profiles = Profile::latest()->get();

        return response()->json([
            'success' => true,
            'message' => 'Data daftar profiles berhasil ditampilkan',
            'data'    => $profiles
        ]);
    }

    public function store(Request $request)
    {
        $allRequest = $request->all();
        
        $validator = Validator::make($allRequest , [
            'umur' => 'required',
            'bio' => 'required',
            'alamat' => 'required',
        ]);

        if($validator->fails()){
            return response()->json($validator->errors() , 400);
        }

        
        $profile = Profile::create([
            'umur' =>  $request->umur,
            'bio' => $request->bio,
            'alamat'=>  $request->alamat,
        ]);

        if($profile){
            return response()->json([
                'success'   => true,
                'message'   => 'Data Profile berhasil dibuat',
                'data'      =>  $profile
            ], 200);
        }

        return response()->json([
            'success'   => false,
            'message'   => 'Data Profile gagal dibuat'
        ], 409);

    }  

    public function show($id)
    {
        $profile = Profile::find($id);

        if($profile)
        {
            return response()->json([
                'success' => true,
                'message' => 'Data profile berhasil ditampilkan',
                'data'    => $profile
            ], 200);
        }

        return response()->json([
            'success' => false,
            'message' => 'Data dengan id : ' .  $id . '  tidak ditemukan',
        ], 404);

    }

    public function update(Request $request , $id)
    {
        $allRequest = $request->all();

        $validator = Validator::make($allRequest, [
            'umur' => 'required',
            'bio' => 'required',
            'alamat' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }
    
        $profile = Profile::find($id);

        if($profile)
        {
            $user = auth()->user();

            if($profile->user_id != $user->id)
            {
                return response()->json([
                    'success' => false,
                    'message' => 'Data profile bukan milik user login',
                ] , 403);

            }

            $profile->update([
                'umur' =>  $request->umur,
                'bio' => $request->bio,
                'alamat'=>  $request->alamat,    
            ]);
            return response()->json([
                'success' => true,
                'message' => 'Data profile berhasil diupdate',
                'data' =>    $profile
            ]);
        }

        return response()->json([
            'success' => false,
            'message' => 'Data dengan id : '. $id .' tidak ditemukan',
        ], 404);
    }

    public function destroy($id)
    {
        $profile = Profile::find($id);

        if ($profile) {
            $user = auth()->user();

            if ($profile->user_id != $user->id) {
                return response()->json([
                    'success' => false,
                    'message' => 'Data profile bukan milik user login',
                ], 403);
            }

            $profile->delete();

            return response()->json([
                'success' => true,
                'message' => 'Data profile berhasil dihapus',
            ], 200);
        }

        return response()->json([
            'success' => false,
            'message' => 'Data dengan id : ' .  $id . '  tidak ditemukan',
        ], 404);
    }


    public function get_profile_user(){
        
        $user = auth()->user();
        $user = User::find($user->id);
        $followings=$user->followings()->count();
        $followers=$user->followers()->count();

        $user_publish=array();
        $user_publish['user_id']=$user->id;
        $user_publish['name']=$user->name;
        $user_publish['username']=$user->username;
        $user_publish['email']=$user->email;
        $profile=Profile::where('user_id', $user->id)->first();
        $user_publish['id']=$profile->id;
        $user_publish['umur']=$profile->umur;
        $user_publish['bio']=$profile->bio;
        $user_publish['alamat']=$profile->alamat;
        $user_publish['followings']=$followings;
        $user_publish['followers']=$followers;
            
        return response()->json([
            'success'   => true,
            'data'      =>  $user_publish
        ], 200);

    }

}

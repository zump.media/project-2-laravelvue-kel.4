<?php

namespace App;

use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Model;

class Komentar extends Model
{
    //
    protected $table='komentars';
    protected $primaryKey='id';
    protected $keyType='string';
    public $incrementing=false;
    
    protected $fillable = [
        'komentar', 'user_id', 'postingan_id'
    ];

    protected static function boot()
    {
        parent::boot();
        static::creating(function($model){
          if (empty($model->{$model->getKeyName()})){
            $model->{$model->getKeyName()}= Str::uuid();
          }
          $model->user_id = auth()->user()->id;

        });
        
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function postingan()
    {
        return $this->belongsTo('App\Postingan');
    }
    
    public function komentar_like(){
        return $this->hasMany('App\KomentarLike');
    }

    public function re_komentar(){
        return $this->hasMany('App\ReKomentar');
    }
   
}
